package measurement;

public class converterBMI {
	
	/** This method calculate Body Mass Index*/
	public double convertBMI(double height, double weight) {
		return weight / (height * height);
	}

}