package measurement;

public class converterBMI {

	/** This method calculate Body Mass Index */
	public double convertBMI(double height, double weight) {

		if (height <= 0 || weight <= 0) {
			throw new IllegalArgumentException("body metrics can not be non-positive!!");
		}

		return weight / (height * height);
	}

}