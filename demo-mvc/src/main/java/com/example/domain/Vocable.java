package com.example.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
//@Table(name="VOCABLE")
public class Vocable {

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	//@Column(name = "id")
	private Long id;
	
	@NotNull
	@Size(min=1)
	@Column(name = "word")
	private String word;
	
	@NotNull
	@Size(min=1)
	//@Column(name = "mean")
	private String mean;

	public Vocable() {

	}
	
	public Vocable(String word, String mean) {
		this.word = word;
		this.mean = mean;
	}

	public Vocable(Long id, String word, String mean) {

		this.id = id;
		this.word = word;
		this.mean = mean;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public String getMean() {
		return mean;
	}

	public void setMean(String mean) {
		this.mean = mean;
	}

	@Override
	public String toString() {
		return "Random szópáros: " + word + " jelentése: " + mean;
	}

}
