package com.example.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
//@Table(name = "meaning")
public class Meaning {
	
	@GeneratedValue
	@Id
	private Long id;

	private String meaning;
	
	@OneToOne
	//@JoinColumn(name = "vocable-id")
	private Vocable voc;

	private Meaning () {
		
	}
	
	public Meaning(Long id, String meaning, Vocable voc) {
		this.id = id;
		this.meaning = meaning;
		this.voc = voc;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMeaning() {
		return meaning;
	}

	public void setMeaning(String meaning) {
		this.meaning = meaning;
	}

	public Vocable getVoc() {
		return voc;
	}

	public void setVoc(Vocable voc) {
		this.voc = voc;
	}

	@Override
	public String toString() {
		return "Meaning [id=" + id + ", meaning=" + meaning + ", voc=" + voc + "]";
	}
	
	
	
}
