package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import com.example.repository.VocableRepository;
import com.example.domain.Vocable;

@Service
public class VocableService {

	private VocableRepository vocableRepo;

	@Autowired
	public void setRepo(VocableRepository repo) {
		this.vocableRepo = repo;
	}

	public List<Vocable> getVocable() {
		return vocableRepo.findAll();
	}

	public Vocable getRandomWord() {
		return getWord(getRandomWordId());
	}

	public Long getRandomWordId() {
		return (long) ((Math.random() * vocableRepo.count()) + 1);
	}

	public Vocable getWord(long wordId) {
		return vocableRepo.findOne(wordId);
	}

	// public List<String> getVocable() {
	//
	// // List<Vocable> voc = repo.findAll();
	//
	//
	// //List<String> ls2 = repo.findMeaning();
	// return ls;
	// }
	//
	// public List<String> getMean() {
	// List<String> ls2 = vocableRepo.findMean();
	// return ls2;
	// }

}
